import datetime

class Record:

    def __init__(self, date, time, location, status, staff):
        self.__id = datetime.datetime.now().timestamp()
        print('The date is ' + date)
        self.__date = date
        self.__time = time
        self.__staff = staff
        self.__location = location
        self.__status = status
    def get_id(self):
        return self.__id
    def get_date(self):
        return self.__date
    def get_time(self):
        return self.__time
    def get_staff(self):
        return self.__staff
    def get_location(self):
        return self.__location
    def get_status(self):
        return self.__status
