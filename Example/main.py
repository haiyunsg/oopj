from flask import Flask, render_template, request, redirect, url_for

import shelve
from Record import Record
from AddRecordForm import *

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('home.html')

@app.route('/record', methods=['GET', 'POST'])
def record():
    form = AddRecordForm(request.form)
    print('The method is ' + request.method)
    if request.method == 'POST':
        if form.validate() == False:
            print('All fields are required.')
        else:
            record_dict = {}
            db = shelve.open('storage.db', 'c')
            try:
                record_dict = db['Records']
            except:
                print("fail to open database")
            new_record = Record(form.date.data, form.time.data, form.location.data, form.status.data, form.staff.data)
            record_dict[new_record.get_id()] = new_record
            db['Records'] = record_dict
            db.close()
            return redirect(url_for('summary'))

    return render_template('record.html', form=form)

@app.route('/summary')
def summary():
    dictionary = {}
    db = shelve.open('storage.db', 'r')
    dictionary = db['Records']
    db.close()

    # convert dictionry to list
    list = []
    for key in dictionary:
        item = dictionary.get(key)
        #print("here: ", user.get_userID())
        #print("here:", user.get_firstname())
        list.append(item)
    return render_template('summary.html', records=list, count=len(list))

if __name__ == "__main__":
    app.run(debug=True, port=8080)