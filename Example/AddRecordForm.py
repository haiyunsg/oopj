from wtforms import Form, StringField, TextAreaField, RadioField, SelectField, validators, PasswordField, SubmitField

class AddRecordForm(Form):
    date = StringField('Date', [validators.Length(min=1, max=150), validators.DataRequired()])
    time = StringField('Time', [validators.Length(min=1, max=150), validators.DataRequired()])
    status = RadioField('Status', choices=[('G', 'Good'), ('F', 'Fail'), ], default='F')
    location = SelectField('Location', [validators.DataRequired()],
                         choices=[('', 'Select'), ('AMK', 'Ang Mo Kio'), ('YCK', 'Yio Chu Kang'), ('Bishan', 'Bishan')], default='')
    staff = TextAreaField('Staff Name', [validators.DataRequired()])
    submit = SubmitField("Add Record")