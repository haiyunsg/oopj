import time
from datetime import datetime
import shelve


class Event:
    def __init__(self, id, name, image, time, desc=''):
        self.__id = id
        self.__name = name
        self.__image = image
        self.__time = time
        self.__desc = desc

    def get_id(self):
        return self.__id

    def get_name(self):
        return self.__name

    def get_image(self):
        return self.__image

    def get_time(self):
        return self.__time

    def get_date_edit_str(self):
        return self.__time.strftime('%d/%m/%y')

    def get_time_edit_str(self):
        return self.__time.strftime('%H:%M')

    def get_time_str(self):
        return self.__time.strftime('%d %b, %H:%M')

    def get_desc(self):
        return self.__desc

    @staticmethod
    def gen_id():
        return int(time.time() * 1000)

    @staticmethod
    def get_dt_from_str(date, time):
        return datetime.strptime(date + ', ' + time, '%d/%m/%y, %H:%M')

    @staticmethod
    def get_dict():
        db = shelve.open('storage')
        # initialize db with default data
        if 'events' not in db:
            db['events'] = Event.get_default_dict()
        dict = db['events']
        db.close()
        return dict

    @staticmethod
    def get_list():
        dict = Event.get_dict()
        return dict.values()

    @staticmethod
    def get_upcoming_list():
        upcoming_list = []
        list = Event.get_list()
        now = datetime.now()
        for event in list:
            if event.get_time() > now:
                upcoming_list.append(event)
        return upcoming_list

    @staticmethod
    def by_time(event):
        return event.get_time()

    @staticmethod
    def get_list_sort_by_time(list):
        sorted_list = sorted(list, key=Event.by_time)
        return sorted_list

    @staticmethod
    def save_dict(dict):
        db = shelve.open('storage')
        db['events'] = dict
        db.close()

    @staticmethod
    def edit(event):
        dict = Event.get_dict()
        dict[event.get_id()] = event
        Event.save_dict(dict)

    @staticmethod
    def delete(id):
        dict = Event.get_dict()
        dict.pop(id)
        Event.save_dict(dict)

    @staticmethod
    def get_event(id):
        dict = Event.get_dict()
        event = dict.get(id)
        return event

    @staticmethod
    def get_default_dict():
        dict = {}
        e1 = Event(
            1,
            'Guitar Club Annual Concert',
            'https://www.nyp.edu.sg/content/dam/nyp/campus-life/cca-activities-and-events/student-clubs-and-interest-groups/events/2018/guitar-annual-concert/thumb_guitar_concert.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg',
            datetime.strptime('23/11/18, 19:00', '%d/%m/%y, %H:%M'),
            'NYP’s Guitar Club is excited to present their annual concert titled “It’s Showtime!” The concert repertoire will be brought to life by our young and passionate guitarists. Guitar Club hopes to make the concert a blockbuster as it showcases to you 13 movie-themed songs through a series of guitar ensembles, duets, groups, bands and collaboration pieces with SoundCard and Piano Ensemble.')
        dict[e1.get_id()] = e1
        e2 = Event(
            2,
            'Food Safety In Action',
            'https://www.nyp.edu.sg/content/dam/nyp/schools-course/scl/events/2018/foodsafety/foodsafety.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg',
            datetime.strptime('29/11/18, 08:45', '%d/%m/%y, %H:%M'),
            'Nanyang Polytechnic (NYP) is once again organising a Food Safety Day – ‘Tackling Food Safety Threats with Innovative Solutions’ -  bringing together different experts in the field and regulators to talk about management of food safety. There will also be facility tours and demonstration of the solutions.')
        dict[e2.get_id()] = e2
        e3 = Event(
            3,
            'NYP Groove! Street Dance Competition',
            'https://www.nyp.edu.sg/content/dam/nyp/campus-life/school-events/2018/nypgroove/groove-eventbrite-event-page-banner.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg',
            datetime.strptime('03/12/18, 10:00', '%d/%m/%y, %H:%M'),
            'Think you’ve got the moves to thrill and rock an audience? Round up your squad and show off your cool steps at the NYP Groove! street dance competition. Gather 3 to 10 members to register today and stand to win attractive prizes!')
        dict[e3.get_id()] = e3
        e4 = Event(
            4,
            'NYP Jam! Singing Competition',
            'https://www.nyp.edu.sg/content/dam/nyp/campus-life/school-events/2018/nypjam/nyp-jam-event-listing-banner.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg',
            datetime.strptime('04/12/18, 09:00', '%d/%m/%y, %H:%M'),
            'Does performing on stage give you a rush? Come flex your musical muscle at the NYP Jam! singing competition and stand to win attractive prizes. Go solo or gather your squad and dazzle us in a group!')
        dict[e4.get_id()] = e4
        return dict