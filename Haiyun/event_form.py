from flask_wtf import FlaskForm
from wtforms import Form, StringField, TextAreaField, RadioField, SelectField, PasswordField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length

class EditEventForm(FlaskForm):
    name = StringField('Name', [Length(min=5, max=100), DataRequired()])
    date = StringField('Date (dd/mm/yy)', [Length(min=8, max=8), DataRequired()])
    time = StringField('Time (HH:MM)', [Length(min=5, max=5), DataRequired()])
    image = TextAreaField('Image URL', [DataRequired()])
    desc = TextAreaField('Descriptions', [DataRequired()])
    submit = SubmitField("OK")


class FilterEventForm(FlaskForm):
    time_filter = SelectField('Time', [DataRequired()],
        choices = [('All', 'All'), ('Upcoming', 'Upcoming')], default='All')
    submit = SubmitField("Filter")
