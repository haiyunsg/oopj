from flask import Flask, flash, redirect, url_for, render_template, request, session
from flask_bootstrap import Bootstrap
import os

from user import User
from event import Event
from event_form import EditEventForm, FilterEventForm

app = Flask(__name__)
bootstrap = Bootstrap(app)


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    elif request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if User.authenticate(username, password):
            session['username'] = username
            session['logged_in'] = True
            user = User.get_user(username)
            session['is_admin'] = user.get_admin()
            return redirect(url_for('home'))
        else:
            flash('Invalid username or password')
            return redirect(url_for('login'))


@app.route("/logout")
def logout():
    session['logged_in'] = False
    session['username'] = None
    session['is_admin'] = False
    return redirect(url_for('home'))


@app.route('/profile')
def profile():
    if session['logged_in']:
        username = session['username']
        user = User.get_user(username)
        return render_template('profile.html', user=user)
    else:
        return redirect(url_for('login'))


@app.route('/events', methods=['GET', 'POST'])
def events():
    form = FilterEventForm(request.form)
    if request.method == 'POST':
        time_filter = form.time_filter.data
        if time_filter == 'Upcoming':
            list = Event.get_upcoming_list()
        else:
            list = Event.get_list()
    else:
        list = Event.get_list()
    sorted_list = Event.get_list_sort_by_time(list)
    print(form.time_filter.data)
    return render_template('events.html', form=form, events=sorted_list)


@app.route('/event/<int:id>', methods=['GET', 'POST'])
def event(id):
    if request.method == 'POST':
        Event.delete(id)
        print('delete event')
        return redirect(url_for('events'))
    event = Event.get_event(id)
    return render_template('event.html', event=event)


@app.route('/event/add', methods=['GET', 'POST'])
def event_add():
    form = EditEventForm(request.form)
    if form.validate_on_submit():
        name = form.name.data
        date = form.date.data
        time = form.time.data
        image = form.image.data
        desc = form.desc.data
        id = Event.gen_id()
        dt = Event.get_dt_from_str(date, time)
        event = Event(id, name, image, dt, desc)
        print(name)
        Event.edit(event)
        print('add event')
        return redirect(url_for('event',id=id))
    return render_template('event_edit.html', title='Add Event', form=form)


@app.route('/event/update/<int:id>', methods=['GET', 'POST'])
def event_update(id):
    form = EditEventForm(request.form)
    if form.validate_on_submit():
        name = form.name.data
        date = form.date.data
        time = form.time.data
        image = form.image.data
        desc = form.desc.data
        dt = Event.get_dt_from_str(date, time)
        event = Event(id, name, image, dt, desc)
        Event.edit(event)
        print('update event')
        return redirect(url_for('event', id=id))
    elif request.method == 'GET':
        event = Event.get_event(id)
        form.name.data = event.get_name()
        form.date.data = event.get_date_edit_str()
        form.time.data = event.get_time_edit_str()
        form.image.data = event.get_image()
        form.desc.data = event.get_desc()
    return render_template('event_edit.html', title='Update Event', form=form)


@app.route('/map')
def map():
    return render_template('map.html')


@app.route('/test')
def test():
    return render_template('test.html')


@app.route('/test2')
def test2():
    return render_template('test2.html')


if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True, port=8081)
