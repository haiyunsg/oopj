import time
from datetime import datetime


def gen_id():
    return int(time.time()*1000)


def get_dt_from_str(date, time):
    return datetime.strptime(date + ', ' + time, '%d/%m/%y, %H:%M')


print(gen_id())
print(get_dt_from_str('23/12/18', '19:00'))
