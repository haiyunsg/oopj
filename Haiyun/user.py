import shelve


class User:
    def __init__(self, username, password, name, email, admin=False):
        self.__username = username
        self.__password = password
        self.__name = name
        self.__email = email
        self.__admin = admin

    def get_username(self):
        return self.__username

    def get_password(self):
        return self.__password

    def get_name(self):
        return self.__name

    def get_email(self):
        return self.__email

    def get_admin(self):
        return self.__admin

    @staticmethod
    def get_dict():
        db = shelve.open('storage')
        # initialize db with default data
        if 'users' not in db:
            db['users'] = User.get_default_dict()
        dict = db['users']
        db.close()
        return dict

    @staticmethod
    def get_default_dict():
        dict = {}
        user1 = User('haiyun', 'password', 'Haiyun', 'hy@gmail.com', True)
        dict[user1.get_username()] = user1
        user2 = User('alex', 'password', 'Alex', 'alex@gmail.com')
        dict[user2.get_username()] = user2
        return dict

    @staticmethod
    def save_dict(dict):
        db = shelve.open('storage')
        db['users'] = dict
        db.close()

    @staticmethod
    def get_user(username):
        dict = User.get_dict()
        user = dict.get(username)
        return user

    @staticmethod
    def authenticate(username, password):
        user = User.get_user(username)
        if user is not None:
            if user.get_password() == password:
                return True
        return False
